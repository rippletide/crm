from flask import Flask
from APP.extensions import db, login_manager, mail
from APP.config import Config, DB_NAME
from os import path


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    # Initialize Flask extensions
    db.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    login_manager.login_veiw = 'login'

    # Import and register blueprint routes
    from APP.Site import Site
    app.register_blueprint(Site, url_prefix="/")

    from APP.auth import auth
    app.register_blueprint(auth, url_prefix="/")

    from APP.admin import admin
    app.register_blueprint(admin, url_prefix="/admin")

    from APP.errors.handlers import errors
    app.register_blueprint(errors, url_prefix="/")


    from APP import models

    create_database(app)


    return app


def create_database(app):
    if not path.exists(DB_NAME):
        with app.app_context():
            db.create_all()
        print('Created Database!')
