from flask import render_template, url_for, request, redirect, flash
from APP.admin.forms import AddUser, TestForm
from APP.auth.forms import LoginForm, SetPasswordForm
from APP.models.admin import Users, Test
from APP.auth import auth
from APP import db, login_manager
from flask_login import login_user, login_required, logout_user

########################################
# Authentication
########################################

@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


# Register Super Admin
# This route is for intial registration of the intial Super Admin
# Once an intial registration has happened this route will no longer be fuctional
@auth.route('/initialization', methods=['GET', 'POST'])
def intial_superadmin():
    form = Initalization
    return render_template('admin/initialization.html', form=form)



#Admin Login
@auth.route('admin/login', methods=['GET', 'POST'])
def admin_login():
    # if current_user.is_authenticated:
        # return redirect(url_for('admin.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(email=form.email.data).first()
        if user and check_password_hash(user.passphrase, form.password.data):
                login_user(user, remember=form.remember.data)
                return redirect(url_for('admin.home'))
        else:
            flash('Login in unsuccessful. Try again.')

    return render_template('authentication/sign-in/admin_login.html', form=form)


#Password Set
@auth.route('/set-password', methods=['GET', 'POST'])
def set_password():
    form = SetPasswordForm()
    if form.validate_on_submit():
        password_to_set = User.query.filter_by(email=form.email.data).first()
        if user.password_hash == None:
            password_to_set.password = form.password.data

            try:
                db.session.commit()
                flash('Password Submited Successfully!')
                return render_template('authentication/sign-in/set-password.html',
                    form=form,
                    password_to_set=password_to_set)
            except:
                flash('Looks like there was a problem... Try Again!')
                return render_template('authentication/sign-in/set-password.html',
                    form=form,
                    password_to_set=password_to_set)
        else:
            flash('Password already set, try logging in.')
    else:
        return render_template('authentication/sign-in/set-password.html', form=form)


#Password Set Varified
@auth.route('/password-set-success')
def password_set_success():
    return render_template('authentication/sign-in/password-set-success.html')




# Logout
# Logout for admins
@auth.route('admin/logout')   # line 4780 on admin/index.html
def logout():
    logout_user()
    return render_template('authentication/sign-in/admin_login.html')



# Subscriber/Doner Registration. Allows users to create an account.
@auth.route('/create-account', methods=['GET', 'POST'])
def create_account():
    return render_template('authentication/sign-up/create-account.html')



# Reset Paswword Request
@auth.route('/reset-password', methods=['GET', 'POST'])
def request_reset():
    if current_user.is_authenticated:
        return redirect(url_for(admin.home))

    form = RequestPwResetForm()
    return render_template('authentication/sign-in/request-reset.html', form=form)
