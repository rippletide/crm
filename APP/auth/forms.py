from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import InputRequired, Length, Optional
from wtforms.widgets import TextArea



class LoginForm(FlaskForm):
    email = StringField('Email')
    password = PasswordField('Password')
    submit = SubmitField('Sign In')


class SetPasswordForm(FlaskForm):
    email = StringField('Email')
    password1 = PasswordField('Password')
    password2 = PasswordField('Confirm Password')
    submit = SubmitField('Submit')


class Initalization(FlaskForm):
    first_name = StringField('First Name', validators=[InputRequired()])
    last_name = StringField('Last Name', validators=[InputRequired()])
    email = StringField('Email', validators=[InputRequired()])
    phone = StringField('Phone', validators=[InputRequired()])
    address = StringField('Address', validators=[InputRequired()])
    city = StringField('City', validators=[InputRequired()])
    state = StringField('State', validators=[InputRequired()])
    zip = StringField('Zip', validators=[InputRequired()])
    password1 = PasswordField("Password")
    password2 = PasswordField("Password")
    superadmin = BooleanField("Super Admin", default=False, validators=[InputRequired()])
    submit = SubmitField('Save')


# Request Reset Password
class RequestPwResetForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired()])
    submit = SubmitField('Submit')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('No account exist.')


# Reset Password
class ResetPasswordForm(FlaskForm):
    password1 = PasswordField('Password', validators=[InputRequired()])
    password2 = PasswordField('Password', validators=[InputRequired()])
    submit = SubmitField('Reset Password')
