import os
from flask import render_template
from flask_mail import Message
from APP.models.admin import Users
from APP import mail


# Sends email for New Admin User to set Password
# Used in admin.routes.adduser
def set_account_password(name, email):

    user = Users.query.filter_by(email=email).first()

    token = user.get_reset_token()

    msg = Message()
    msg.subject = "Create Password"
    msg.body = "Hello " + name + "," "\n\nPlease use the following link to set a password and activate your account. \
                Once your password is set, your acount will activate and you will be automatically redirected to \
                your account. \
                \n\n"
    msg.sender = os.environ.get('MAIL_USERNAME')
    msg.recipients = [email]
    msg.html = render_template('authentication/sign-in/set-password.html', user=user, token=token)

    mail.send(msg)
