import os

basedir = os.path.abspath(os.path.dirname(__file__))

DB_NAME = 'test.db'



class Config:

    # Configure email settings
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    #MAIL_DEBUG = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')
    MAIL_MAX_EMAILS = 500
    #MAIL_SUPPRESS_SEND = False
    MAIL_ASCII_ATTACHMENTS = False

    # Profile Image Upload Folder
    UPLOAD_FOLDER = '/static/media/avatars/'


    SQLALCHEMY_DATABASE_URI = (f'sqlite:///{DB_NAME}')

    SECRET_KEY = os.environ.get('SECRET_KEY')
