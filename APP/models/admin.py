import os
import jwt
from APP import db, login_manager
from flask_login import UserMixin
from datetime import datetime
from time import time
from werkzeug.security import generate_password_hash, check_password_hash



# Login manager
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user.id))


#  User Database Model
#  Users have Admin Access. May Have Roles. Can Be Super Admin.
class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=True)
    last_name = db.Column(db.String(50), nullable=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    phone = db.Column(db.String(15), nullable=True)
    address = db.Column(db.String(100), nullable=True)
    city = db.Column(db.String(100), nullable=True)
    state = db.Column(db.String(30), nullable=True)
    zip = db.Column(db.String(10), nullable=True)
    country = db.Column(db.String(100), nullable=True)
    profile_img = db.Column(db.String(), nullable=True)
    superadmin = db.Column(db.Boolean, default=False)
    admin = db.Column(db.Boolean, default=True)
    notes = db.Column(db.String(25000), nullable=True)
    date_added = db.Column(db.DateTime, default=datetime.utcnow)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return 'User {}'.format(self.first_name + '' + self.last_name)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute!')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_token(self, expires=86400):
        return jwt.encode({'reset_password': self.email, 'exp': time() + expires},
                            key=os.environ.get('SECRET_KEY'))

    @staticmethod
    def verify_reset_token(token):
        try:
            username = jwt.decode(token, key=os.environ.get('SECRET_KEY'))['reset_password']
            print(first_name + ' ' + last_name)
        except Exception as e:
            print(e)
            return
        return User.query.filter_by(email=email).first()




class Test(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))

    def __repr__(self):
        return f'<Test "{first_name} {last_name}">'
