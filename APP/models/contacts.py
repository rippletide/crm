from APP import db
from flask_login import UserMixin
from datetime import datetime




#  Contact Database
#  Contacts May Have Various Attributes Such As Donor, Volunteer, Etc.
#
class Contacts(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(100), nullable=False, unique=True)
    phone = db.Column(db.String(15), nullable=False)
    address = db.Column(db.String(100), nullable=False)
    city = db.Column(db.String(100), nullable=False)
    state = db.Column(db.String(30), nullable=False)
    zip = db.Column(db.String(10), nullable=False)
    country = db.Column(db.String(100))
    notes = db.Column(db.String(25000))
    password = db.Column(db.String(50), nullable=False)
    date_added = db.Column(db.DateTime, default=datetime.utcnow)

    # Create a String
    def __repr__(self):
        return 'User {}'.format(self.first_name + '' + self.last_name)
