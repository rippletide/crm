# Routes for public facing website pages

##############################

from flask import render_template, request, Blueprint
from APP.Site import Site

# page routing
@Site.route("/")
@Site.route("/index")
def home():
    return render_template('Site/index.html')
