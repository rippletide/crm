from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import StringField, PasswordField, TextAreaField, SubmitField, BooleanField, ValidationError
from wtforms.validators import InputRequired, Length, Email, Optional, EqualTo
from wtforms.widgets import TextArea




class AddUser(FlaskForm):
    first_name = StringField('First Name', validators=[InputRequired(), Length(min=2, max=50)])
    last_name = StringField('Last Name', validators=[InputRequired(), Length(min=2, max=50)])
    email = StringField('Email', validators=[InputRequired()])
    phone = StringField('Phone', validators=[InputRequired()])
    address = StringField('Address', validators=[InputRequired()])
    city = StringField('City', validators=[InputRequired()])
    state = StringField('State', validators=[InputRequired()])
    zip = StringField('Zip', validators=[InputRequired()])
    country = StringField('Country')
    superadmin = BooleanField("Super Admin")
    admin = BooleanField("Admin")
    profile_img = FileField("Profile Image")
    notes = StringField('Note', widget=TextArea(), validators=[Optional(strip_whitespace=True), Length(min=2, max=25000)])
    submit = SubmitField('Submit')


###################################
# Forms For Updating User Info

# Update User Password
class UpdateUser(FlaskForm):
    first_name = StringField('First Name')
    last_name = StringField('Last Name')
    phone = StringField('Phone')
    email = StringField('Email')
    address = StringField('Address')
    city = StringField('City')
    state = StringField('State')
    zip = StringField('Zip')
    note = TextAreaField('Note')
    profile_img = FileField("Profile Image")
    submit_update = SubmitField('Submit')

##################################
# Testing Form

class TestForm(FlaskForm):
    first_name = StringField('First Name')
    last_name = StringField('Last Name')
    submit = SubmitField('Save')
