from flask import render_template, url_for, request, redirect, flash, current_app
from flask_login import login_required
from flask_jwt_extended import jwt_required, create_access_token, get_jwt_identity
from flask_mail import Message
from APP.admin import admin
from APP.admin.forms import AddUser, TestForm, UpdateUser
from APP.models.admin import Users, Test
from APP.email import set_account_password
from APP import db
from werkzeug.utils import secure_filename
import uuid as uuid
import os


##########################################
##########################################
#
# Routes With Admin Access Only
#
##########################################
##########################################



##########################################
# User Management
# Only Super-Admin Access`
##########################################

# Add New Admin User
## Sends email with a tokenized link to new user that allows them
## to set a password for their new account and logs them in.
@admin.route('/add-user/', methods=['GET', 'POST'])
def adduser():
    form = AddUser()
    if form.validate_on_submit():
        user = Users.query.filter_by(email=form.email.data).first()
        if user is None:
            user = Users(first_name=form.first_name.data,
                        last_name=form.last_name.data,
                        email=form.email.data,
                        phone=form.phone.data,
                        address=form.address.data,
                        city=form.city.data,
                        state=form.state.data,
                        zip=form.zip.data,
                        country=form.country.data,
                        notes=form.notes.data,
                        superadmin=form.superadmin.data,
                        admin=True
                        )

            try:
                db.session.add(user)
                db.session.commit()

                flash("A new user was successfully created!", category="success")

            except:
                flash("Something went wrong! Try again.", category="error")

            try:
                # Send Email For Password Set
                set_account_password(form.first_name.data, form.email.data)

            except:
                flash("Email was successfully sent!", category="success")


    name = form.first_name.data
    form.first_name.data = ''
    form.last_name.data = ''
    form.email.data = ''
    form.phone.data = ''
    form.address.data = ''
    form.city.data = ''
    form.state.data = ''
    form.zip.data = ''
    form.country.data = ''
    form.notes.data = ''
    form.superadmin.data = ''

    users = Users.query.order_by(Users.admin, Users.id)
    return render_template('admin/user-management/users/add-user.html',
                            form=form,
                            users=users)





# Veiw Admin User
@admin.route('/view-user/<int:id>', methods=['GET', 'POST'])
def view_user(id):
    user_to_view = Users.query.get_or_404(id)
    form = UpdateUser()
    if form.validate_on_submit():
        user_to_view.first_name = form.first_name.data
        user_to_view.last_name = form.last_name.data
        user_to_view.phone = form.phone.data
        user_to_view.email = form.email.data
        user_to_view.address = form.address.data
        user_to_view.city = form.city.data
        user_to_view.zip = form.zip.data
        user_to_view.zip = form.zip.data
        user_to_view.note = form.note.data
        user_to_view.profile_img = form.profile_img.data

        # Aquire image name
        img_filename = secure_filename(user_to_view.profile_img.filename)
        # Set UUID
        img_name = str(uuid.uuid1()) + "_" + img_filename
        # Save image to file
        user_to_view.profile_img.save(os.path.join(current_app.config['UPLOAD_FOLDER'], img_name))
        # Change image to string to save in db
        user_to_view.profile_img = img_name

        try:
            db.session.commit()

        except:
            flash("Something went wrong! Try again.")


    return render_template('admin/user-management/users/view-user.html', form=form, user_to_view=user_to_view)


##########################################
# Constituent Management
# Only Admin Access
# Admin Access Defind By Group Membership
##########################################

# Admin Dashboard
@admin.route("/")
@admin.route("/index")
#@login_required
def home():
    return render_template('admin/index.html')




# For testing db connection
@admin.route('/test', methods=['GET', 'POST'])
def test():
    form = TestForm()
    if form.validate_on_submit():
        user = Test(first_name=form.first_name.data, last_name=form.last_name.data)
        db.session.add(user)
        db.session.commit()
    flash("User Added Successfully!")
    return render_template('admin/user-management/users/test.html',
                        form=form)
